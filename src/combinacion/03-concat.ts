
/**
 * El concat map nos sirve para ir contatenando
 * observables. Estos se emitiran cuando el elemento
 * anterior se haya completado en caso contrario 
 * jamas llegara a producirse el cambio
 * IMPORTANTE SE DEBE IMPORTAR DE RXJS
 */

import { interval,concat, of} from "rxjs";
import { take } from "rxjs/internal/operators/take";

 const interval$ = interval(1000);


 concat(
    interval$.pipe(take(3)),
    interval$.pipe(take(2)),
    of(1)
 ).subscribe( console.log)