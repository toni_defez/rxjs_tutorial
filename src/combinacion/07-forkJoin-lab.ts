import { forkJoin, of } from "rxjs";
import { ajax } from "rxjs/ajax";
import { catchError } from "rxjs/operators";

const GITHUB_API_URL = 'https://api.github.com/users';
const GITHUB_USER = 'klerith';

forkJoin(
 {
   usuario:ajax.getJSON(
     `${GITHUB_API_URL}/${GITHUB_USER}`),
    respos: ajax.getJSON(
      //Si algunos de los servicios revienta
      //se cogen por el catchError en a y despues en b
      `${GITHUB_API_URL}/${GITHUB_USER}/repdos`
    ).pipe(
      // catchError --> a
      catchError(err =>of([]))
    ),
    gists: ajax.getJSON(
      `${GITHUB_API_URL}/${GITHUB_USER}/gists`
    )
 }
).pipe(
  //catchError --> b
  catchError(err => of(err.message))
).subscribe(console.log)