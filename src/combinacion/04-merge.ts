/**
 * merge (f)
 * Con la funcion merge podemos unificar varios
 * observables y que se vayan emitiendo de forma
 * alterna. Este nuevo observable no se completara
 * hasta que los observables que lo componen se 
 * hayan completado
 * Importante mira el import (rxjs)
 */

import { fromEvent,merge} from "rxjs";
import { pluck } from "rxjs/operators";



const keyUp$ = fromEvent(document,'keyup');
const click$ = fromEvent(document,'click');

merge(
   keyUp$.pipe(pluck('type'))
   ,click$.pipe(pluck('type'))
).subscribe(console.log)