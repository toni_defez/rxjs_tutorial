import { ajax } from "rxjs/ajax";
import { startWith } from "rxjs/operators";

const loadingDiv = document.createElement('div');
loadingDiv.classList.add('loading');
loadingDiv.innerHTML = 'Cargando...';

const body = document.querySelector('body');


//Con este ejercicio estamos haciendo un ejemplo 
// de uso del operador startWith, lo que hacemos
// es dar un valor inicial(boolean) que indique 
// a la pantalla que se esta cargando producto
//Stream
ajax.getJSON('https://reqres.in/api/users/2?delay=3')
.pipe(
   startWith(true)
)
.subscribe(resp =>{

   if(resp === true){
      body.append(loadingDiv);
   }else{
      document.querySelector('.loading').remove();
   }
   console.log(resp)
})


