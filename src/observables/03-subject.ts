import { Observable, Subscriber, Observer, observable, Subject } from 'rxjs';


// el observer 
const observer: Observer<any> = {
    next: value => console.log('siguiente [next]:', value),
    error: error => console.warn('error [obs]:', error),
    complete: () => console.info('completed')
}





const intervalo$ = new Observable<number>(subs =>{

    const interval = setInterval(()=>{
        subs.next(Math.random())
    },1000);

    //el return se ejecuta cuando nos hacemos un
    // unsubscribe 
    return (()=>{
        clearInterval(interval);
        console.log("Interval destruido")
    })
});

/**
 * SUBJECT CARACTERISTICAS
 *  1-Casteo multiple : Muchas suscripciones van a estar sujetas a este
 *    observable y van a recibir la misma informacion 
 *  2- Tambien es un observer
 *  3- Next, error y complete
 * 
 */
const subject$ = new Subject();
//estamos relacionando el subject ()
const subscription = intervalo$.subscribe(subject$)



// MUESTRAN VALORES DIFERENTES 
// const subs1=intervalo$.subscribe(resp=>{
//         console.log("Subs1",resp);
// });

// const subs2=intervalo$.subscribe(resp=>{
//     console.log("Subs2",resp);
// });


// Con el subject esta sincronizadas y muestran el mismo
// valor

const subs1 = subject$.subscribe(observer)

const subs2 = subject$.subscribe(observer)

setTimeout(()=>{

    // Estamos metiendo desde fuera el valor  para que se suscriban
    // el resto de elemento
    subject$.next(10);
    subject$.complete();
    subscription.unsubscribe();
},3500)


/***
 * Un subject nos permite convertir un Cold -> Hot observable
 * Cuando la data es producida por el observable en sí mismo
 * , es considerado un "Cold Observable".Pero cuando la data 
 * es producido FUERA del observable es llamado "HotObservable"
 */