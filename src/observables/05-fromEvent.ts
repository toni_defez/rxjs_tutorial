import {fromEvent, Subject} from 'rxjs';

/**
 * Eventos del DOM
 */

 let cadena:string="";

 const src1$ = fromEvent<MouseEvent>(document,'click');
 const src2$ = fromEvent<KeyboardEvent>(document,'keyup');

const observer = {
    next:val=>console.log('next',val)
};

// en vez de hacer resp.x,resp.y 
// puedo hacer esto directamente
src1$.subscribe(({x,y}) =>{
    console.log(x);
    console.log(y);
});

// en vez de hacer resp.key
// En vez de esto
//src2$.subscribe(resp=>{ console.log(resp.key)})
// hago esto
//src2$.subscribe(({key})=>{console.log(key)})
src2$.subscribe(({key})=>{

    if(key == 'Backspace'){
       cadena = cadena.slice(0,cadena.length-1); 
    }
    else if(key=='CapsLock'){
        
    }
    else{
        cadena+=key;
    }
    console.log(cadena);
   
 
});