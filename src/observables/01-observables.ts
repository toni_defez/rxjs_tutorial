import { Observable, Subscriber, Observer } from 'rxjs';
import { SafeSubscriber } from 'rxjs/internal/Subscriber';

const observer : Observer <any>= {
    next: value => console.log('siguiente [next]:',value),
    error: error => console.warn('error [obs]:',error),
    complete : ()=> console.info('completed')
}

const obs$ = new Observable<string>(subs => {
    subs.next('Hola');
    //Forzar error
    const a = undefined;
    a.nombre = 'Fernando';

    subs.complete(); // si no hago el complete jamas se producira el complete en el subscribe
});


// obs$.subscribe(
//     valor => console.log('next', valor),
//     error => console.warn('error', error),
//     () => console.info('completado')
// );

obs$.subscribe(observer);
obs$.subscribe();



// el subscribe tiene varias formas de ser consumidas

//1)De esta manera lo que estamos consumiendo es directamente el 
// el objeto next del observable
obs$.subscribe(resp=>{
    console.log(resp);
})

//2) Con tres callback
obs$.subscribe(
    valor => console.log('next',valor),
    error => console.log('err',error),
    ()=>console.log("Completado")
);

//3) Con un observer

// Definicio del observer1
const observer1 : Observer <any>= {
    next: value => console.log('siguiente [next]:',value),
    error: error => console.warn('error [obs]:',error),
    complete : ()=> console.info('completed')
};
// uso del suscribe
obs$.subscribe(observer);









