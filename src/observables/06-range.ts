import {range,of,asyncScheduler} from 'rxjs';

//como el of es sincrono
//const src$= of(1,2,3,4,5,6);

//range tiene dos parametros
// 1) Valor inicial
// 2) Numero de interacciones
//const obs1$ = range (-5,10) --> emite del -5 hasta el 4 ( contando el 0)
const obs1$ = range(1,10);

//src$.subscribe(console.log)
// console.log("Inicio")
// obs1$.subscribe(resp=>{
//     console.log(resp);
// });
// console.log("Fin");

console.log("Ahora con asyncronia");
//Podemos hacer que el range/of se comporte
// con asyncronia y eso lo hacemos de la siguiente manera

// USO DE asyncScheduler

const obs2$ = range(1,100,asyncScheduler);

console.log('Inicio')
obs2$.subscribe(console.log);
console.log('Fin');
