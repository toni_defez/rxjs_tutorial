import { interval, timer } from 'rxjs';

const observer = {
    next: val => console.log('next:', val),
    complete: () => console.log('complete')
}


/** interval
 * emite valores segun el tiempo especificado
 * y no para de emitirlos
 * 
 */
const interval$ = interval(1000);

/**
 * timer
 * Emite el valor cuando pasa el tiempo especificados
 * y se completa
 */
//const timer$ = timer(2000);


const hoyEn5 = new Date();// ahora 
hoyEn5.setSeconds(hoyEn5.getSeconds() + 5);

// Con dos valores 
// 1) Cuando empieza
// 2) El periodicidad
const timer$ = timer(hoyEn5, 1000);
console.log('inicio')

timer$.subscribe(observer);
//interval$.subscribe(observer);
console.log('fin');




