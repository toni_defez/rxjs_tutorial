import { Observable, Subscriber, Observer, observable } from 'rxjs';
import { SafeSubscriber } from 'rxjs/internal/Subscriber';

const observer: Observer<any> = {
    next: value => console.log('next:', value),
    error: error => console.warn('error :', error),
    complete: () => console.info('completed')
}



const intervalo$ = new Observable<number>(subscriber => {
    let num = 0;
    const interval = setInterval(() => {
        subscriber.next(num++);
        console.log(num);
    }, 1000);

    setTimeout(()=>{
        subscriber.complete()
    },3000);

    return ()=>{
        clearInterval(interval);
        console.log('Intervalo destruido');
    }
})


const subs1 = intervalo$.subscribe(observer);
const subs2 = intervalo$.subscribe(observer);
const subs3 = intervalo$.subscribe(observer);

subs1.add(subs2).add(subs3);

setTimeout(
    () => {
        subs1.unsubscribe()
        console.log('Completado timeout');
    },
    3000);
