import {of} from 'rxjs';


//El of produce un observable sincrono 
// const obs$ = of(1,2,3,4,5,6);
//El of convierte la secuencia  de argumentos que recibe
// valores que fluyen a traves del observable
const obs$ = of<number>(...[1,2,3,4,5,6])
//const obs$ = of<any>([1,2,3,4],{a:"1"},function(){},true,Promise.resolve(true))

console.log('Inicio');
obs$.subscribe(
    next =>console.log('next',next),
    null,
    ()=>console.log('Terminamos la secuencia')
)
console.log('Fin')