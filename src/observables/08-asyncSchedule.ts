/**
 * Recuerda!! 
 * El asynSchedule no crea un observable  crea una subscripcion
 * Una subscription es el producto de un suscribe
 * 
 *  Conclusiones
 *  El asyncScheduler nos permite trabajar creando intervalos 
 *  de forma simple ademas de darnos las posibilidad de trabajar
 *  con recursividad.
 *  
 */

import {asyncScheduler} from 'rxjs';

//setTimeout(() => {}, 3000);
//setInterval(()=>{},3000)

const saludar = ()=>{console.log("Hola mundo")};
const saludar2 = (nombre)=>{console.log(`Hola ${nombre}`)};


// El state es el parametro que le pasamos a la funcion 
//asyncScheduler.schedule(saludar2,2000,{nombre:'pepe'});

const subs =  asyncScheduler.schedule( function(state){
    console.log('state',state);
    this.schedule(state+1,3000);
},3000,0);


// setTimeout(() => {
//     subs.unsubscribe();
// }, 12000); 

//La ventaja del asyncScheduler es que es una subscripcion y podemos
// hacer cosas


asyncScheduler.schedule(()=>subs.unsubscribe(),6000);