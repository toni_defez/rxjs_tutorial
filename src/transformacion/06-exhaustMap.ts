
/**
 * ExhaustMap (operador de aplanamiento)
 * Este operarador se caracteriza por que no permite
 * que se encolen otras observables.
 * Unicamente acceptara observables si no hay ninguna
 * delante, emitiendose en caso contrario sera ignorado.
 * Es util cuando tenemos observables que emiten muchos
 * valores y podemos ignorar
 */

import { fromEvent } from "rxjs/internal/observable/fromEvent";
import { take, exhaustMap } from "rxjs/operators";
import { interval } from "rxjs";

const click = fromEvent(document,'click');

const interval$ = interval(500).pipe(
   take(3)
);

click.pipe(
    exhaustMap(()=> interval$)
).subscribe({
   next:value => console.log(value),
   complete:()=>console.log('complete')
})