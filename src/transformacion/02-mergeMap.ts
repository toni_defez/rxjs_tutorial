import {of, interval, fromEvent} from 'rxjs';
import { mergeMap, take, map, mergeAll, takeUntil } from 'rxjs/operators';

const letras$ = of('a','b','c')

letras$.pipe(
   mergeMap((letra)=> interval(1000).pipe(
      map( i =>letra + i),
      take(3)
   ))
)
/**
.subscribe({
   next:val => console.log('next:',val),
   complete:()=>console.log('Complete')
})
**/

const mousedown$ = fromEvent(document,'mousedown');
const mouseup$ = fromEvent(document,'mouseup');
const interval$ = interval();

/** 
 * El mergeMap es un operador de aplanamiento
 * esto quiere decir que siempre vamos a obtener 
 * FirstOrder Observable.
 * Lo que nos permite el mergeMap es llamar a otro
 * observable (interval) y emitir el el resultado
 * en el principal sin ningun problema
 */
mousedown$.pipe(
   mergeMap(()=> interval$.pipe(
      takeUntil(mouseup$)
   ))
).subscribe(console.log);