import { fromEvent, merge, Observable, of } from "rxjs";
import { debounceTime, map, pluck, mergeAll, catchError, filter, isEmpty, tap, mergeMap, switchMap } from "rxjs/operators";
import { ajax } from "rxjs/ajax";
import { GitHubUsersResp, GitHubUser } from "../interfaces/github-user.interface";

/**
 * Operadores de aplanamiento
 * Cuando ven que retornan un observable realmente
 * no van ese obserbale , sino lo que retornarian
 * cuando fluyen a traves de el sera el resultado 
 * de la suscripcion interna
 */




//Referencias
const body = document.querySelector('body');
const textInput = document.createElement('input');
const orderList = document.createElement('ol');
body.append(textInput, orderList);

// Helpers 
const mostrarUsuarios = (usuarios:GitHubUser[])=>{
   orderList.innerHTML = '';

   if(usuarios.length>0){
      for ( const usuario of usuarios){
         const li = document.createElement('li');
         const img = document.createElement('img');
         img.src = usuario.avatar_url;
   
         const anchor = document.createElement('a');
         anchor.href = usuario.html_url;
         anchor.text = 'Ver página';
         anchor.target = '_blanck';
         li.append(img);
         li.append(usuario.login +'');
         li.append(anchor);
   
         orderList.append(li);   }
   }
   else {
      const p = document.createElement('p');
      p.innerHTML = `<h1>No hay usuarios con ese nombre </h1>`;
      orderList.append(p);
   }
   
   
  
};


//Streams
const input$ = fromEvent<KeyboardEvent>(textInput, 'keyup').pipe(

);


//Conceptos nuevos 
/**
 * Observables HighOrder 
 *  Se llama asi al observable que emite a su vez otro 
 *  observable
 * Observable FirstOrder
 *  Se llama asi al observable estandar , que emite un 
 *  valor normal
 * 
 * MergeAll
 *  Converts a higher-order Observable into a
 *  first-order Observable which
 *  concurrently delivers all values that are 
 *  emitted on the inner Observables.
 *  Se utiliza de una forma bastante tosca,
 *  Se pone justo despues del pipeo que 
 *  devuelve un observable 
 */
input$.pipe(
   debounceTime<KeyboardEvent>(500),
   pluck<KeyboardEvent,string>('target','value'),
   mergeMap<string,Observable<GitHubUsersResp>>( texto => ajax.getJSON
      (`https://api.github.com/search/users?q=${texto}`
      )),
  // mergeAll(),
      pluck<GitHubUsersResp,GitHubUser[]>('items'),
      catchError(err =>{
         console.log("Error");
         return of([]);
      })
     
)//.subscribe(mostrarUsuarios)


const url = 'https://httpbin.org/delay/1?arg=';

/**
 * MergeMap
 * (operador de aplanamiento)
 * El mergeMap nos permite estar escuchando
 * multiples observables que se estan emitiendo
 * desde el original. No se completaran hasta
 * que no se complete el principal. Haciendo
 * que se vayan emitiendo varios next de distintos 
 * observables
 */


 /**
  * SwitchMap
  * (operador de aplanamiento)
  *  Hace lo mismo que el Mergemap , pero este
  *  unicamente hace que los observables secundarios
  *  se vayan cancelando si aparece uno mas reciente,
  *  Quitandole llamadas inecesarias
  *  Prueba a cambiar el mergeMap/SwithMap en el siguiente
  *  ejemplo 
  */


input$.pipe(
   pluck('target','value'),
   switchMap<string,Observable<any>>(resp =>
      ajax.getJSON(url+resp))
).subscribe(console.log)