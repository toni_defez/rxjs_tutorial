import { fromEvent,interval } from "rxjs";
import { mergeMap, switchMap } from "rxjs/operators";


const click$ = fromEvent(document,'click');
const interval$ = interval(1000);

/**
 * MergeMap
 * Se pueden estar emitiendo valores varios
 * de diferentes observables
 * SwitchMap
 * Unicamente se emitira valores de un unico 
 * observable y este sera el mas recientes. 
 * Los anteriores son cancelados
 */
click$.pipe(
   switchMap(()=>interval$)
).subscribe(console.log);