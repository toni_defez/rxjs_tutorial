import { interval, fromEvent } from "rxjs";
import { take, switchMap, map, concatMap } from "rxjs/operators";

const interval$ = interval(500).pipe(
   take(3)
   
);

/**
 * ConcatMap
 * El concatMap es un operador de aplanamiento
 * Este a diferencia del MergeMap ( emision simultanea)
 * y el SwitchMap(unicamente emite el ultimo) tiene un 
 * comportamiento FIFO con los observables que van 
 * metiendose en una cola. Produciendo que el primero que se emita
 * tenga que completarse para que el segundo empieze,
 * el segundo y el resto se van encolando a la espera
 * de que se vayan emitiendo
 */


const click = fromEvent(document,'click');


click.pipe(
    concatMap(()=> interval$)
).subscribe({
   next:value => console.log(value),
   complete:()=>console.log('complete')
})



