// creando un formulario

import { fromEvent, of } from "rxjs";
import { tap, map, pluck, switchMap, mergeMap, catchError, exhaustMap } from "rxjs/operators";
import { ajax } from "rxjs/ajax";

const url = 'https://reqres.in/api/login?delay=1';
const form = document.createElement('form');
const inputEmail = document.createElement('input');
const inputPass = document.createElement('input');
const submitBtn = document.createElement('button');


//Helpers
const peticionHttpLogin = (userPass)=>
ajax.post(url,userPass).pipe(
   pluck('response','token'),
   catchError( err => of('xxxx'))
) ;


//Configuraciones
inputEmail.type = 'email';
inputEmail.placeholder = 'email';
inputEmail.value = 'eve.holt@reqres.in';

inputPass.type = 'password';
inputPass.placeholder = 'password';
inputPass.value = 'cityslicka';
submitBtn.innerHTML = 'Ingresar';

form.append(
   inputEmail, inputPass, submitBtn
);

document.querySelector('body').append(
   form
);

// Strems

const submitForm$ = fromEvent<Event>(form, 'submit')
.pipe(
   tap(ev => ev.preventDefault()),
   map(ev =>({
      email: ev.target[0].value,
      password: ev.target[1].value
   })),
  // mergeMap(peticionHttpLogin),
  // switchMap(peticionHttpLogin),
   exhaustMap(peticionHttpLogin),
);

submitForm$.subscribe(token=>console.log(token))