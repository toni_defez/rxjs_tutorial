/**
 * El sample time nos permite obtener a nosotros una subscripcion 
 * que esta pendiente de cada una de sus emisiones en un periodo
 * de tiempo
 */

import { fromEvent } from "rxjs";
import { map, sample, sampleTime } from "rxjs/operators";

const click$ = fromEvent<MouseEvent>(document, 'click');

click$.pipe(
   sampleTime(2000),
   map(({ x, y }) => ({ x, y }))
).subscribe(console.log)