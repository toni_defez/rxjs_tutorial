import { fromEvent, asyncScheduler } from "rxjs";
import {throttleTime, debounceTime, pluck, distinctUntilChanged } from "rxjs/operators";



const click$ = fromEvent(document,'click');

//Ejemplo 1
click$.pipe(throttleTime(3000))//.subscribe(console.log);

//Ejemplo 2 
const input = document.createElement('input');
document.querySelector('body').append(input);

const input$ = fromEvent(input,'keyup').pipe(
   // map<any,any>(({target})=>({value:target.value}))

   throttleTime(1000,asyncScheduler,{
      leading:true,
      trailing:true
   }),
   pluck('target','value'),
   distinctUntilChanged()
);


input$.subscribe(console.log);