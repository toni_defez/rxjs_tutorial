import { fromEvent } from "rxjs";
import { debounceTime, map, pluck, distinctUntilChanged } from "rxjs/operators";

const click$ = fromEvent(document,'click');

/*
DebounceTime:
Añade un tiempo minimo entre valores cambiantes de una emision
En el caso expuesto del input no se realizara la llamada hasta que el 
valor del input este sin alterar el tiempo especificado
*/

//Ejemplo 1
click$.pipe(debounceTime(3000))//.subscribe(console.log);

//Ejemplo 2 
const input = document.createElement('input');
document.querySelector('body').append(input);

const input$ = fromEvent(input,'keyup').pipe(
   // map<any,any>(({target})=>({value:target.value}))

   debounceTime(1000),
   pluck('target','value'),
   distinctUntilChanged()
);


input$.subscribe(console.log);