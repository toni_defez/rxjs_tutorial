import { interval, fromEvent } from "rxjs";
import { sample } from "rxjs/operators";

const interval$ = interval(500);
const click$ = fromEvent(document,"click");

/*
Sample
 En este caso lo que observamos es que si no se 
 emite un valor del evento click$ no se emite ningun
 valor del interval. Por lo tanto lo que hace el sample
 es crear la depencia del interval sobre el click
*/


interval$.pipe(
   sample(click$)
).subscribe(console.log)