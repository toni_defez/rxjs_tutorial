import { fromEvent } from "rxjs";
import { auditTime, tap, map } from "rxjs/operators";

const click$ = fromEvent<MouseEvent>(document, 'click');

/*
   auditTime
   Coge el ultimo valor emitido en el espacio
   de tiempo marcado. Los anteriores son ignorados.
   
 */
click$.pipe(
   map(({ x, y }) => ({ x, y })),
   tap(val => console.log('tap', val)),
   auditTime(2000)
).subscribe(console.log);