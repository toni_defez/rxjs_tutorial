import { ajax, AjaxError } from 'rxjs/ajax';
import { of } from 'rxjs';
import { catchError } from 'rxjs/operators';



const url = 'https://httpvbin.org/delay/1';

const manejaError = (resp:AjaxError)=>{
   console.warn('error',resp.message)
   return of({
      ok:false,
      usuarios:[]
   })
}

// const obs2$ = ajax.getJSON(url).pipe(
//    catchError(manejaError)
// );

// const obs1$ = ajax(url).pipe(
//    catchError(manejaError)
// );


const obs2$ = ajax.getJSON(url);
const obs1$ = ajax(url);

obs1$.
pipe(
   catchError(manejaError)
).
subscribe(
   {
      next:val => console.log('next',val),
      error:err => console.log('error en subs',err),
      complete:()=>console.log('complete')
   }
);
//obs2$.subscribe(data => console.log('data ajax:', data));