import { of } from "rxjs/internal/observable/of";
import { distinctUntilKeyChanged } from "rxjs/operators";

interface Personaje {
    nombre: string;
}


const personajes = of<Personaje>(
    {
        nombre: "Toni"
    },
    {
        nombre: "Toni"
    },
    {
        nombre: 'Pep'
    },
    {
        nombre: "Luis"
    },
    {
        nombre: "Toni"
    }
);

personajes.
    pipe(
        distinctUntilKeyChanged('nombre')
    ).
    subscribe(console.log)