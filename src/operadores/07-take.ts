import { from, of } from "rxjs";
import { take, tap } from "rxjs/operators";


const numeros$ =of(1,2,3,4,5,6,7).pipe(
    tap(console.log)
);



//el take limita el numero de valores 
// que van a ser emitidos por el observable
// de esta manera unicamente se emitira 
// hasta el valor 3
numeros$.pipe(
    take(3)
).subscribe({
    next:val => console.log('next:',val),
    complete: () => console.log('complete')
})