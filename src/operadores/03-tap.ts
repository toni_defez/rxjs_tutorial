//TAP
//El principal uso del tap es lanzar efectos
// secundario que no tenga que ver con lo mencionado
// nos sirve para debugear y controlar los valores que se
// emitiendo


import { range } from 'rxjs';
import { tap, map } from 'rxjs/operators';



const numeros = range(1,5);


numeros.
pipe(
  tap( x =>{
      console.log('antes',x);
      return 100;
}),
map(val => (val*10)),
tap({
    next: valor=>console.log('despues',valor),
    complete: ()=>console.log('Se terminó todo')
})
)
.subscribe(val => console.log('subs',val))