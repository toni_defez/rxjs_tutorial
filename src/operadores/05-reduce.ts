import { interval } from "rxjs";
import {take, reduce, tap} from 'rxjs/operators';

const number = [1, 2, 3, 4, 5]


const totalReducer = (acumulador: number, valorActual: number) => {
    return acumulador + valorActual;
}

const total = number.reduce(totalReducer, 0);
//console.log('total arreglo', total);

interval(1000).pipe(
    take(4)  // se completa cuando se llega al numero de emisiones 
            // indicadas
    ,tap((val)=>console.log('tap',val))
    , reduce( totalReducer,5) 
    // acumula los valores emitidos y hace que el 
    // unicamente se emita una subscripcion 
    // y esta es la producia por el reduce  

).subscribe({
    next: val => console.log('next:', val),
    complete: () => console.log('complete')
}

)