import { filter, map } from 'rxjs/operators'
import { range, of, from, fromEvent} from 'rxjs';


range(1,10).pipe(
    filter((resp,index)=> resp%2!==0)
)//.subscribe(console.log);

interface character {
    tipo:string;
    nombre:string;
}

const personajes:character[] = [
    {
        tipo:'hero',
        nombre:'Batman'
    },
    {
        tipo:'hero',
        nombre:'SuperMan'
    },
    {
        tipo:'hero',
        nombre:'Green Lantern'
    },
    {
        tipo:'villano',
        nombre:'Joker'
    }
];
let soloHeroes = from(personajes).pipe(
    filter<any>(resp=> resp.tipo == 'hero')
);
soloHeroes.subscribe(resp=>console.log(resp));


const keyUp$ = fromEvent<KeyboardEvent>(document, 'keyup');

keyUp$.pipe(
    //filter(({key})=>key == 't')
    map( event =>event.code ), //recibe un keyBoardEvent y emite un string
    filter( key => key === 'Enter')
    ).subscribe(resp=>console.log(resp))