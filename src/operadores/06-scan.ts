import { from, of } from "rxjs";
import { map, reduce, scan, pluck } from "rxjs/operators";

const numeros = [1, 2, 3, 4, 5];

// const totalAcumulador = (acc,cur)=>{
//     return acc + cur;
// }

const totalAcumulador = (acc, cur) => acc + cur;



// como hemos comentado anteriomente 
// unicamente emitimos el resultado 
// total del observable /// reduce 
from(numeros).pipe(
    reduce(totalAcumulador, 0)
).subscribe(console.log);


// con el scan lo que conseguimos es
// ir emitidiendo todos las transformaciones
// segun van ocurriendo  // scan 
from(numeros).pipe(
    scan(totalAcumulador, 0)
).subscribe(console.log);


//Redux
//Simulacion de comportamiento del patron redux
// Evolucion del estado del usuario
interface Usuario {
    id?: string;
    autenticado?: boolean;
    token?: string;
    edad?: number;
}

//creamos un array de usuario 
const users: Usuario[] = [
    { id: 'fher', autenticado: false, token: null },
    { id: 'fher', autenticado: true, token: 'ABC' },
    { id: 'fher', autenticado: true, token: 'ABC123' }
];


// al usar el from este observable esta haciendo tres emisiones
// de cada uno de los elementos del array
const state$ = from(users).pipe(
    scan<Usuario>((acc, cur) => {
        // este escan lo que esta haciendo es actualizar la informacion que
        // que vamos recibiendo 
        return { ...acc, ...cur }
    },{edad:33}
    // con un atributo inicial que hemos puesto aqui
    )
);

// nos suscribimos al state y ademos de ello lo que hacemos 
// es mapear el objeto del mismo 
const id$ = state$.pipe(
   // map(state =>state.id)
);

// nos suscribimos a los posibles c
id$.subscribe(console.log)