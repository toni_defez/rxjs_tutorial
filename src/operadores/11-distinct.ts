import { of } from "rxjs";
import { distinct } from "rxjs/operators";

//Distinct
//Miramos los valoes que se han emitido
//si anteriormente se ha emtido el valor
// este no se volvera a emitir NUNCA


const numeros = of<number|string>('1',1, 2, 3, 4, 5, 6, 1, 2, 7);



numeros.pipe(
    distinct() //// usa el ===
).subscribe(console.log);


interface Personaje {
    nombre:string;
}


const personajes = of<Personaje>(
    {
        nombre:"Toni"
    },
    {
        nombre:'Pep'
    },
    {
        nombre:"Luis"
    },
    {
        nombre:"Toni"
    }
);

personajes.
pipe(
    distinct(p => p.nombre)
).
subscribe(console.log)