import { of } from "rxjs";
import { distinctUntilChanged } from "rxjs/operators";


//DistinctUntilChanged
//Emite valor si es diferente al ultimo emitido.
// Distinct
// 1,1,2,3,1 ---> se emite 1,2,3
// El uno ya se habia emitido y por lo tanto 
// JAMAS se volvera a emitir
//DistinctUntilChange
//1,1,2,3,1 ---> se emite 1,2,3,1
//El segundo uno no se emite por que es igual al ultimo
// valor emitido , pero si se emite el ultimo por que es 
// diferente al 3.


const numeros = of<number | string>('AB', 1, 2, 3, 4, 5, 6, 1, 2, 7, 'AB');



numeros.pipe(
    distinctUntilChanged() //// usa el ===
).subscribe(console.log);


interface Personaje {
    nombre: string;
}


const personajes = of<Personaje>(
    {
        nombre: "Toni"
    },
    {
        nombre: "Toni"
    },
    {
        nombre: 'Pep'
    },
    {
        nombre: "Luis"
    },
    {
        nombre: "Toni"
    }
);

personajes.
    pipe(
        distinctUntilChanged((ant, act) => ant.nombre === act.nombre)
    ).
    subscribe(console.log)