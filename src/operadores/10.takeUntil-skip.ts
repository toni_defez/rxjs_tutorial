import { fromEvent, interval } from "rxjs";
import { tap, map, first, takeUntil, skip } from "rxjs/operators";


//EJEMPLO MIO 
//Se basa en dos 


// const keyUp$ = fromEvent<KeyboardEvent>(document,'keyup').pipe(
//  //   tap<KeyboardEvent>(val =>console.log('tap',val.key)),
//     map(({key})=>(key)),
//  //   tap((val)=>console.log(val)),
//     first(key => key === "T")
// )

// const click$ = fromEvent(document,'click').pipe(takeUntil(keyUp$));


// keyUp$.subscribe({
//     next:val =>console.log('keyBoardEvent:',val),
//     complete:()=>console.log('Complete keyBoard')
// });

// click$.subscribe({
//     next:val =>console.log('clickEvent:',val),
//     complete:()=>console.log('Complete Click')
// })


const boton = document.createElement('button');
boton.innerHTML = 'Detener Timer';


document.querySelector('body').append(boton);

const counter$ = interval(1000);
const clickBtn$ = fromEvent(boton, 'click').
    pipe(
        tap(() => console.log('tap antes del skip')),
        skip(2),  //el skip detiene el flujo de ejecucion
        //este no se producira hasta que el skip se rompa
        tap(() => console.log('tap despues del skip'))
    );

//EL TAKE UNTIL NOS PERMITE IR EMITIENDO LOS VALORES DEL COUNTER
// HASTA QUE EL CLICKBTN EMITA SU PRIMER VALOR 
// EN ESE MOMENTO EL COUNTER SE COMPLETARA
counter$.pipe(
    takeUntil(clickBtn$)
).subscribe({
    next: val => console.log('next', val),
    complete: () => console.log('complete')
})