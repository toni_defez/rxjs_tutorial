import {range, fromEvent,} from 'rxjs';
import { map,pluck, mapTo }  from 'rxjs/operators'

/**
 * Los operadores deben de ir precedidos por el 
 * uso de pipe gracias a este podemos hacer modificaciones
 * Siempre que podamos debemos identificar los
 * valores que se usan para  en el map
 * map<number,number>
 * --> primero entrante valor
 * --> segundo valor de salida
 */

// range(1, 5)f
//     .pipe(
//         map<number, number>(val => val * 10)
//     ).subscribe(console.log)



const keyUp$ = fromEvent<KeyboardEvent>(document,'keyup')
const keyUpCode$ = keyUp$.pipe(map(event=>event.key));

/** Pluck nos permite obtener el atributo que queremos
 * exactamente
 */
const keyUpPluk$ = keyUp$.pipe(
    pluck('target','baseURI')
);

/**
 * MapTo
 * nos permite convertir cualquier entrada en 
 * una salida concreta
 */

 const KeyUpMapTo$ = keyUp$.pipe(
     mapTo('tecla presionada')
 )


keyUpPluk$.subscribe(resp=>console.log(resp));
keyUpCode$.subscribe(resp=>console.log(resp));
KeyUpMapTo$.subscribe(resp=>console.log(resp));