import { fromEvent } from "rxjs";
import { first, map, tap } from "rxjs/operators";

// con el operador first hacemos que se emita el primer valor
// que cumpla un requisito( el predicate), si no llegamos a introducir
// ninguno el subscription parara con la primera emision 
//Cuando se cumpla el first este valor se emitira de la misma manera el observable se completara


const clicks$ = fromEvent<MouseEvent>(document, 'click').pipe(
    //  first()  // === take (1)
    //   map<MouseEvent,number>((item)=>item.clientY),
    //tap<MouseEvent>(console.log),
    // map(event => ({
    //     clientY:event.clientY,
    //     clientX:event.clientX
    // }))
    map(({ clientX, clientY }) => ({ clientY, clientX }))
    , first((value) => value.clientY >= 150)
);

clicks$.subscribe({
    next: (val) => console.log('next:', val),
    complete: () => console.log('complete')
});

