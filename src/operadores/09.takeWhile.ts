import { fromEvent } from "rxjs";
import { map, takeWhile } from "rxjs/operators";


const click$ = fromEvent<MouseEvent>(document,'click');

//El takeWhile filtra valores hasta que el predicate del mismo ya no se cumpla en ese momento se completa la observable
// tenemos el parametro inclusive que nos indica si el valor esta dentro o no 

click$
.pipe(
    map(({x,y})=>({x,y})),
    takeWhile(({y})=>y<=150,true)
)
.subscribe({
    next:val => console.log('next',val),
    complete: ()=>console.log('complete')
})