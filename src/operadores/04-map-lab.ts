import { from, fromEvent } from "rxjs";
import { map, tap } from "rxjs/operators";

const texto = document.createElement('div');

texto.innerHTML = `
Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque tempor lacus quis malesuada eleifend. Fusce consequat justo in justo cursus pulvinar. Suspendisse non euismod felis. Phasellus rhoncus augue id tincidunt accumsan. Suspendisse elementum pulvinar ligula, elementum tincidunt felis. Morbi commodo vestibulum ipsum, vel molestie justo vehicula eget. Ut sollicitudin sed turpis quis fermentum. Maecenas volutpat nulla nibh, eu fermentum augue pellentesque commodo. Integer dapibus condimentum sapien euismod pretium. Integer hendrerit pellentesque erat mattis consectetur. Aliquam vel metus et metus laoreet condimentum. Etiam erat dui, pellentesque nec tincidunt vel, bibendum porttitor diam. Vivamus eu rhoncus nisi. Morbi ullamcorper diam ac laoreet tristique.
<br/><br/>
Maecenas lobortis mi at elit placerat, sed suscipit risus tincidunt. Donec lobortis lorem vitae mauris ultrices, consequat tempus diam maximus. Donec ultrices efficitur nibh, vel suscipit risus vestibulum sit amet. Nullam sed risus egestas, posuere dui non, volutpat ante. Integer ut enim id magna pharetra pellentesque ac dignissim dui. Cras consequat lacus sed luctus blandit. Vestibulum nisl ex, rutrum vel nulla egestas, viverra ultrices est. Vestibulum eget aliquam nisi, at commodo justo. Praesent a sodales urna, vel faucibus tortor. Praesent tincidunt molestie erat sit amet iaculis.
<br/><br/>
Nunc lectus augue, molestie aliquet porttitor ac, aliquam non ipsum. Donec at nunc vehicula nisi commodo mollis vel quis orci. Morbi vehicula dapibus lectus, at molestie urna luctus vitae. Etiam ultrices condimentum nisl, vel auctor magna mollis eu. Nulla iaculis cursus diam a eleifend. Fusce sodales tincidunt aliquam. Nunc velit dolor, vehicula et nisl eu, ornare suscipit mi. Vivamus congue sem vel lacus tempor tincidunt. Morbi feugiat orci vitae metus rutrum, et tempus mauris molestie. Morbi et orci felis. Nam porta, odio ac feugiat convallis, nulla enim varius magna, id pulvinar odio orci quis ante. Donec finibus risus sed ullamcorper gravida. Duis non metus sed mi maximus maximus in mollis enim.
<br/><br/>
Nullam sit amet mi dignissim, lobortis elit eu, condimentum elit. Nunc id enim diam. Sed molestie dui tristique libero aliquam, in semper justo congue. Aliquam erat volutpat. Morbi id aliquet dui. Phasellus semper, justo non imperdiet facilisis, lectus magna ullamcorper sapien, ut aliquam neque nulla ut nibh. Morbi nisl orci, consequat luctus sagittis id, consectetur id augue. Proin dapibus velit non fringilla tempor. Aenean luctus sollicitudin eros, ut convallis diam. Cras vestibulum lorem felis.
<br/><br/>
Ut sagittis id justo quis lobortis. Morbi vulputate efficitur odio at bibendum. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Quisque egestas, mauris vel commodo lacinia, eros turpis pharetra est, fringilla ultricies augue orci sollicitudin sem. Quisque auctor nisl eget imperdiet efficitur. Donec ut condimentum diam. Cras efficitur tempor nibh ac cursus. Suspendisse semper rhoncus diam. Nam feugiat aliquet leo, a scelerisque lorem consectetur at. Vivamus feugiat, sem sed sollicitudin consequat, sem massa aliquam massa, egestas pharetra purus massa lobortis nulla. Nulla vel mollis purus, id ornare tellus. Integer sit amet nisi ultrices, tristique sapien et, semper augue. Mauris vitae nibh et velit sagittis sollicitudin semper vitae diam. Cras eget sem libero. Sed pharetra nisl ac lacinia egestas.
<br/><br/>
Mauris tincidunt dui ac mauris accumsan, eget dapibus orci mattis. Phasellus a dui sapien. Pellentesque interdum id turpis et scelerisque. Aliquam sit amet suscipit sem. Donec pellentesque, ex non molestie imperdiet, ante nunc laoreet nibh, in egestas turpis elit at ante. Mauris at cursus sem. Ut sit amet lobortis lectus. Proin volutpat, risus vitae ultrices tristique, velit sapien molestie urna, volutpat rutrum odio ipsum at felis. Etiam non felis pellentesque, vehicula arcu tincidunt, ultrices felis. Praesent quam tortor, dapibus nec vestibulum non, porttitor vel urna. Donec convallis blandit lectus, et imperdiet nibh congue sit amet. Nullam vel iaculis arcu, quis maximus purus. Nam dignissim quam non rutrum convallis.
<br/><br/>
In facilisis tempus tristique. Mauris porta neque sed felis efficitur, maximus ullamcorper dolor sodales. Donec blandit et metus et malesuada. Aenean eu euismod nisl, mattis viverra nisi. Praesent vel consequat erat. Curabitur commodo aliquam accumsan. Integer vestibulum elit eu dolor commodo, vel posuere tellus maximus.
<br/><br/>
Vivamus pretium lacinia leo, a semper tortor ornare at. Suspendisse sed convallis ipsum. Maecenas vehicula facilisis euismod. Mauris sed auctor dolor. Fusce non orci eget ante aliquam consectetur. Mauris erat odio, hendrerit id diam in, vestibulum semper urna. Vestibulum tincidunt viverra venenatis.
<br/><br/>
Aenean pharetra ligula eu justo efficitur, ac pretium nibh finibus. Sed lacus lectus, pellentesque ut ullamcorper sit amet, scelerisque vulputate ipsum. Pellentesque at condimentum orci. Phasellus maximus libero et sapien euismod scelerisque. Duis pharetra a augue sit amet vestibulum. Aliquam orci diam, finibus ac varius eget, tristique sed risus. Sed mattis porta elementum. Etiam ac est vitae libero tincidunt lacinia ac non massa. Integer tincidunt luctus massa, at tincidunt arcu posuere vitae. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Mauris venenatis varius aliquet.
<br/><br/>
Nunc hendrerit, eros non lobortis ullamcorper, nunc nisi bibendum diam, sed suscipit ligula tortor a tortor. Phasellus feugiat ultrices est. Aenean rutrum, velit sed bibendum porta, arcu sem tincidunt turpis, at interdum diam justo at orci. Fusce vestibulum condimentum nisi nec posuere. Suspendisse facilisis nunc eget aliquet scelerisque. Aliquam porttitor augue et pulvinar blandit. Vivamus bibendum sodales arcu, sit amet mollis nulla dignissim in. Integer lorem erat, rhoncus non accumsan in, suscipit ac erat. Vivamus suscipit vestibulu
`;


const body = document.querySelector('body');
body.append(texto);

const progressBar = document.createElement('div');
progressBar.setAttribute('class','progress-bar');
body.append(progressBar);


// funcion que haga el calculo
const calcularPorcentajeScroll = (event)=>{
    const {
        scrollTop,
        scrollHeight,
        clientHeight
    } = event.target.documentElement;
   
    return (scrollTop / (scrollHeight - clientHeight))*100;
};

//streams

const scroll$ = fromEvent<Event>(document,'scroll');
//scroll$.subscribe(resp=>console.log(resp));

const progress$ = scroll$.pipe(
    map(calcularPorcentajeScroll),
    tap(console.log));

progress$.subscribe(porcentaje =>{
    progressBar.style.width = `${porcentaje}%`;
});

